<?php

namespace App\Entity\Planning;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Formulaire\User;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Planning\CreneauRepository")
 */
class Creneau
{

    const OCCUPER = "OCCUPER";
    const LIBRE = "LIBRE";

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Disponibilite")
     * @ORM\JoinColumn(name="Disponibilite_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $disponibilite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\Column(type="time")
     */
    private $hDebut;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formulaire\User")
     * @ORM\JoinColumn(name="User_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getHDebut(): ?\DateTimeInterface
    {
        return $this->hDebut;
    }

    public function setHDebut(\DateTimeInterface $hDebut): self
    {
        $this->hDebut = $hDebut;

        return $this;
    }

    /**
     * @param mixed $disponibilite
     */
    public function setDisponibilite($disponibilite): void
    {
        $this->disponibilite = $disponibilite;
    }

    /**
     * @return mixed
     */
    public function getDisponibilite()
    {
        return $this->disponibilite;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}

<?php

namespace App\Repository\Formulaire;

use App\Entity\Formulaire\ReponseNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReponseNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReponseNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReponseNumber[]    findAll()
 * @method ReponseNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseNumberRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReponseNumber::class);
    }

//    /**
//     * @return ReponseNumber[] Returns an array of ReponseNumber objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReponseNumber
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Form\Formulaire;

use App\Entity\Formulaire\ReponseDate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date',
                DateType::class,
                array('years' => range(date('Y') - 21, date('Y') + 20),
                    'placeholder' => array('year' => 'Year', 'month' => 'Month', 'day' => 'Day'),
                    "required" => false,
                    'format' => 'dd-MM-yyyy',
                    "label" => "date limite (laisser à  'Month / Day / Year' si vous voulez que la date soit toujours égal à la date d'aujourdhui)")
                );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReponseDate::class,
        ]);
    }
}

<?php

namespace App\Entity\Planning;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Planning\DisponibiliteRepository")
 */
class Disponibilite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Creneau[]
     * @ORM\OneToMany(targetEntity="Creneau", cascade={"persist","remove"}, mappedBy="disponibilite")
     */

    public $creneaux;

    /**
     * @ORM\ManyToOne(targetEntity="Planning")
     * @ORM\JoinColumn(name="Planning_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $planning;

    /**
     * @ORM\Column(type="time")
     * @Assert\LessThan(propertyPath="hFin")
     */
    private $hDebut;

    /**
     * @ORM\Column(type="time")
     * @Assert\GreaterThan(propertyPath="hDebut")
     */
    private $hFin;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHDebut()
    {
        return $this->hDebut;
    }

    public function setHDebut($hDebut): self
    {
        $this->hDebut = $hDebut;

        return $this;
    }

    public function getHFin()
    {
        return $this->hFin;
    }

    public function setHfin($hFin): self
    {
        $this->hFin = $hFin;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @param mixed $planning
     */
    public function setPlanning($planning): void
    {
        $this->planning = $planning;
    }

    /**
     * @return mixed
     */
    public function getPlanning()
    {
        return $this->planning;
    }

    /**
     * @param Creneau[] $creneaux
     */
    public function setCreneaux(array $creneaux): void
    {
        $this->creneaux = $creneaux;
    }
}

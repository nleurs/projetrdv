<?php

namespace App\Form\Formulaire;

use App\Entity\Formulaire\SecurityUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('role',ChoiceType::class,['label' => 'choix du role:', 'choices'  => array(
                'gestionnaire' => "ROLE_GEST",
                'administrateur' => "ROLE_ADMIN",
            ),])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SecurityUser::class,
        ]);
    }
}

<?php

namespace App\Entity\Formulaire;

use App\Entity\Planning\Creneau;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;


/**
 * @ORM\Entity(repositoryClass="App\Repository\Formulaire\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timeRdv;

    /**
     * @var UserReponse[]
     * @ORM\OneToMany(targetEntity="UserReponse", cascade={"persist","remove"}, mappedBy="user")
     */
    public $userReponses;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $nbReponse;


    /**
     * @var Creneau[]
     * @ORM\OneToMany(targetEntity="App\Entity\Planning\Creneau", cascade={"persist","remove"}, mappedBy="user")
     */
    public $creneaux;

    public function __construct()
    {
        $this->userReponses = new ArrayCollection();
        $this->ordre = new Integer(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeRdv(): ?int
    {
        return $this->timeRdv;
    }

    public function setTimeRdv(?int $timeRdv): self
    {
        $this->timeRdv = $timeRdv;

        return $this;
    }

    public function addReponse(UserReponse $r)
    {
        $this->userReponses[] = $r;
    }

    public function checkMax(Reponse $r)
    {
        if ($this->getTimeRdv() < $r->getBesoinRdv()) {
            $this->setTimeRdv($r->getBesoinRdv());
        }
    }

    public function incNbReponse()
    {
        $this->nbReponse++;
    }

    public function decNbReponse()
    {
        $this->nbReponse--;
    }
}

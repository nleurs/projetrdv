<?php

namespace App\Controller\Formulaire;


use App\Entity\Formulaire\Question;

use App\Form\Formulaire\QuestionType;

use App\Repository\Formulaire\FormRepository;
use App\Repository\Formulaire\QuestionRepository;
use App\Repository\Formulaire\ReponseRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class QuestionControllerAdmin extends AbstractController
{

    /**
     * @Route("/showQuestionAdmin/{id}", name="showQuestionAdminId")
     * @Route("/showQuestionAdmin", name="showQuestionAdmin")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function showQuestionAdmin($id = null, FormRepository $repoForm, QuestionRepository $repoQuestion)
    {
        $form = $repoForm->findOneBy([]);
        $listQuestion = $repoQuestion->findAll();
        if (!$form) {
            return $this->redirectToRoute('createForm');
        }

        if ($id === null) {
            if ($form->getFirstQuestion() === null) {
                return $this->render('question/AssignFirstQuestion.html.twig', [
                    'controller_name' => 'QuestionControllerAdmin',
                    'repoQuestion' => $listQuestion,

                ]);
            } else {
                $id = $form->getFirstQuestion();
            }
        }

        $question = $repoQuestion->find($id);
        if ($question->type === Question::CHOICE) {
            return $this->render('question/showQuestionAdmin.html.twig', [
                'controller_name' => 'QuestionControllerAdmin',
                'question' => $question,
                'repoQuestion' => $listQuestion,

            ]);
        }

        if ($question->type === Question::NUMBER) {
            return $this->render('question/showQuestionAdminNumber.html.twig', [
                'controller_name' => 'QuestionControllerAdmin',
                'question' => $question,
                'repoQuestion' => $listQuestion,

            ]);
        }

        if ($question->type === Question::TEXT) {
            return $this->render('question/showQuestionAdminText.html.twig', [
                'controller_name' => 'QuestionControllerAdmin',
                'question' => $question,
                'repoQuestion' => $listQuestion,

            ]);
        }

        if ($question->type === Question::DATE) {
            foreach ($question->reponses as $rep) {
                if ($rep->actu === true) {
                    $rep->reload();
                }
            }
            return $this->render('question/showQuestionAdminDate.html.twig', [
                'controller_name' => 'QuestionControllerAdmin',
                'question' => $question,
                'repoQuestion' => $listQuestion,

            ]);
        }
        dump($question);
        die;
    }

    /**
     * @Route("/assignFirstQuestion", name="assignFirstQuestion")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function assigneFirstQuestion(QuestionRepository $repoQuestion, FormRepository $repoForm, ObjectManager $manager)
    {
        $form = $repoForm->findOneBy([]);
        $id = $_GET['question'];
        $question = $repoQuestion->find($id);
        $form->setFirstQuestion($question->getId());
        $manager->persist($form);
        $manager->flush();
        return $this->redirect("/showQuestionAdmin/$id");
    }

    /**
     * @Route("/createQuestion", name="createQuestion")
     * @Route("/createQuestion/{id}", name="createQuestionId")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createQuestion($id = null, Request $request, ObjectManager $manager, FormRepository $repoForm, ReponseRepository $repoReponse)
    {
        $form = $repoForm->findOneBy([]);
        $question = new Question;
        $formQuestion = $this->createForm(QuestionType::class, $question);

        $formQuestion->handleRequest($request);

        if ($formQuestion->isSubmitted() && $formQuestion->isValid()) {

            $manager->persist($question);
            $manager->flush();

            if ($form->getFirstQuestion() === null) {
                $form->setFirstQuestion($question->getId());
            }
            $manager->persist($form);
            $manager->flush();

            if ($id != null) {
                $reponse = $repoReponse->find($id);
                $reponse->setSuivante($question->id);
                $manager->persist($reponse);
                $manager->flush();

            }

            return $this->render('reponse/SelectTypeReponse.html.twig', [
                'controller_name' => 'ReponseControllerAdmin',
                'id' => $question->id,
            ]);
        }
        return $this->render('question/createQuestion.html.twig', [
            'controller_name' => 'QuestionControllerAdmin',
            'formQuestion' => $formQuestion->createView(),
        ]);
    }

    /**
     * @Route("/createSuivante/{id}", name="createSuivante")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createSuivante($id, QuestionRepository $repoQuestion, ReponseRepository $repoReponse)
    {
        $reponse = $repoReponse->find($id);
        $repoQuestion = $repoQuestion->findall();
        return $this->render('question/createSuivante.html.twig', [
            'controller_name' => 'QuestionControllerAdmin',
            'reponse' => $reponse,
            'repoQuestion' => $repoQuestion,

        ]);
    }

    /**
     * @Route("/assignSuivante/{id}", name="assignSuivante")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function assignSuivante($id, QuestionRepository $repoQuestion, ReponseRepository $repoReponse, ObjectManager $manager)
    {
        $reponse = $repoReponse->find($id);
        $idq = $_GET['question'];
        $question = $repoQuestion->find($idq);
        $reponse->setSuivante($question->id);
        $manager->persist($reponse);
        $manager->flush();
        return $this->redirect("/showQuestionAdmin/$idq");
    }

    /**
     * @Route("/changeQuestion/{id}", name="changeQuestion")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function changeQuestion($id, Request $request, ObjectManager $manager, QuestionRepository $repoQuestion)
    {
        $question = $repoQuestion->find($id);

        $formQuestion = $this->createForm(QuestionType::class, $question);

        $formQuestion->handleRequest($request);

        if ($formQuestion->isSubmitted() && $formQuestion->isValid()) {
            $manager->persist($question);
            $manager->flush();
            return $this->redirectToRoute('showQuestionAdmin', ['id' => $question->id]);
        }
        return $this->render('question/createQuestion.html.twig', [
            'controller_name' => 'QuestionControllerAdmin',
            'formQuestion' => $formQuestion->createView(),
        ]);
    }

    /**
     * @Route("/deleteQuestion/{id}", name="deleteQuestion")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function deleteQuestion($id, ObjectManager $manager, QuestionRepository $repoQuestion, ReponseRepository $repoReponse, FormRepository $repoForm)
    {
        $question = $repoQuestion->find($id);
        $reponses = $repoReponse->findall();
        $form = $repoForm->findOneBy([]);
        if ($form->getFirstQuestion() === $question->getId()) {
            $form->setFirstQuestion(NULL);
            $manager->persist($form);
            $manager->flush();
        }
        foreach ($reponses as $reponse) {
            if ($reponse->getSuivante() === $question->getId()) {
                $reponse = $repoReponse->find($reponse->getId());
                $reponse->setSuivante(null);
                $manager->persist($reponse);
                $manager->flush();
            }
        }

        $manager->remove($question);
        $manager->flush();
        return $this->redirectToRoute('formAdmin');

    }
}

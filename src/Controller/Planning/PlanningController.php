<?php

namespace App\Controller\Planning;

use App\Entity\Planning\Planning;

use App\Repository\Planning\DisponibiliteRepository;
use App\Repository\Planning\PlanningRepository;
use App\Repository\Planning\SecurityUserRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class PlanningController extends AbstractController
{

    /**
     * @Route("/createPlanning/{id}", name="createPlanning")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function createPlanning($id = null, SecurityUserRepository $repoSecurityUser, ObjectManager $manager)
    {
        $planning = new Planning;
        $securityUser = $repoSecurityUser->find($id);
        $planning->setSecurityUser($securityUser);
        $securityUser->setPlanning($planning);
        $manager->persist($securityUser);
        $manager->flush();

        return $this->redirect("/login");
    }

    /**
     * @Security("is_granted('ROLE_GEST')")
     * @Route("showPlanning", name="showPlanning")
     * @Route("showPlanning/{id}", name="showPlanning")
     */
    public function showPlanning($id = null, SecurityUserRepository $repoSecurityUser, DisponibiliteRepository $repoDispo, PlanningRepository $repoPlanning)
    {

        if ($id === null) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        } else {
            if (($this->get('security.token_storage')->getToken()->getUser()->getRoles()[0] === "ROLE_ADMIN") OR ($this->get('security.token_storage')->getToken()->getUser()->getPlanning()->getId() === (integer)$id)) {
                $planning = $repoPlanning->find($id);
                $user = $repoSecurityUser->findOneBy(array('planning' => $planning));
            } else {
                dump('ACCES DENIED');
                die;
            }
        }

        $dispos = $repoDispo->findBy(array('planning' => $user->getPlanning()->getId()));

        return $this->render('Planning/showPlanning.html.twig', [
            'controller_name' => 'PlanningController',
            'user' => $user,
            'dispos' => $dispos,
        ]);
    }
}

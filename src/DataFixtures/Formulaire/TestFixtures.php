<?php

namespace App\DataFixtures\Formulaire;

use App\Entity\Formulaire\Form;
use App\Entity\Formulaire\Question;
use App\Entity\Formulaire\Reponse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TestFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $form = new Form();
        $manager->persist($form);

        $q1 = new Question();
        $q1->setLibelle("Où êtes-vous inscrit pour l'année universitaire 2018/2019 ? :");
        $q1->setType("CHOICE");
        $q1->setCode("Où êtes-vous inscrit");
        $manager->persist($q1);

        $q2 = new Question();
        $q2->setLibelle("Etiez-vous déjà inscrit dans l'enseignement supérieur en France l'année 2017/2018? :");
        $q2->setType("CHOICE");
        $q2->setCode("Etiez-vous déjà inscrit");
        $manager->persist($q2);

        $q3 = new Question();
        $q3->setLibelle("Quelle est votre Nationalité ? :");
        $q3->setType("CHOICE");
        $q3->setCode("Nationalité");
        $manager->persist($q3);

        $q4 = new Question();
        $q4->setLibelle("Dans quel département résidez vous ? :");
        $q4->setType("CHOICE");
        $q4->setCode("département");
        $manager->persist($q4);

        $q5 = new Question();
        $q5->setLibelle("Quel document de séjour en France possédez-vous? :");
        $q5->setType("CHOICE");
        $q5->setCode("docuement de séjour");
        $manager->persist($q5);

        $q6 = new Question();
        $q6->setLibelle("Dans quel département résidez vous ? :");
        $q6->setType("CHOICE");
        $q6->setCode("département");
        $manager->persist($q6);

        //reponse question 1
        $r = new Reponse();
        $r->setQuestion($q1);
        $r->setLibelle("ULCO ou EILCO");
        $r->setCode("ULCO ou EILCO");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q1);
        $r->setLibelle("ISCID-CO");
        $r->setCode("ISCID-CO");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q1);
        $r->setLibelle("IUT du Littoral Côte d'Opale (Calais)");
        $r->setCode("IUT Calais");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q1);
        $r->setLibelle("IUT du Littoral Côte d'Opale (Boulogne sur Mer ou Dunkerque ou Saint Omer)");
        $r->setCode("IUT Boulogne Dunkerque StOmer)");
        $manager->persist($r);

        //reponse question 2

        $r = new Reponse();
        $r->setQuestion($q2);
        $r->setLibelle("Oui");
        $r->setCode("Oui");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q2);
        $r->setLibelle("Non");
        $r->setCode("Non");
        $manager->persist($r);

        //reponse question 3

        $r = new Reponse();
        $r->setQuestion($q3);
        $r->setLibelle("Algérienne");
        $r->setCode("Algérienne");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q3);
        $r->setLibelle("Autre");
        $r->setCode("Autre");
        $manager->persist($r);

        //reponse question 4

        $r = new Reponse();
        $r->setQuestion($q4);
        $r->setLibelle("Nord(59)");
        $r->setCode("Nord");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q4);
        $r->setLibelle("Pas de calais");
        $r->setCode("Pas de calais");
        $manager->persist($r);

        //reponse question 5

        $r = new Reponse();
        $r->setQuestion($q5);
        $r->setLibelle("Titre de séjour étudiant ou Visa long séjour étudiant (VLS-TS)");
        $r->setCode("séjour étudiant");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q5);
        $r->setLibelle("Titre de séjour Scientifique");
        $r->setCode("séjour scientifique");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q5);
        $r->setLibelle("Autre");
        $r->setCode("Autre");
        $manager->persist($r);

        //reponse question 6

        $r = new Reponse();
        $r->setQuestion($q6);
        $r->setLibelle("Nord(59)");
        $r->setCode("Nord");
        $manager->persist($r);

        $r = new Reponse();
        $r->setQuestion($q6);
        $r->setLibelle("Pas de calais");
        $r->setCode("Pas de calais");
        $manager->persist($r);

        $manager->flush();


    }
}

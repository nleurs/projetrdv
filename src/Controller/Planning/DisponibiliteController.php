<?php

namespace App\Controller\Planning;

use App\Entity\Planning\Creneau;
use App\Entity\Planning\Disponibilite;
use App\Form\Planning\DisponibiliteType;
use App\Repository\Planning\CreneauRepository;
use App\Repository\Planning\DisponibiliteRepository;
use App\Repository\Planning\PlanningRepository;
use App\Repository\Planning\SecurityUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class DisponibiliteController extends AbstractController
{
    /**
     * @Route("/createDisponibilite/{id}", name="createDisponibiliteId")
     * @Route("/createDisponibilite", name="createDisponibilite")
     */
    public function createDisponibilite($id = null, Request $request, ObjectManager $manager, PlanningRepository $repoPlanning, SecurityUserRepository $repoSecurityUser, DisponibiliteRepository $repoDispo)
    {
        if ($id === null) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        } else {
            if ($this->get('security.token_storage')->getToken()->getUser()->getRoles()[0] === "ROLE_ADMIN") {
                $user = $repoSecurityUser->find($id);
            } else {
                dump('ACCES DENIED');
                die;
            }
        }

        $dispo = new Disponibilite();

        $formDispo = $this->createForm(DisponibiliteType::class, $dispo);

        $formDispo->handleRequest($request);

        if ($formDispo->isSubmitted() && $formDispo->isValid()) {
            $planning = $repoPlanning->find($user->getPlanning()->getId());
            $dispo->setPlanning($planning);
            $manager->persist($dispo);
            $manager->flush();
            $idd = $dispo->getId();
            return $this->redirect("/createCreneau/$idd");
//            }
        }
        return $this->render('disponibilite/createDisponibilite.html.twig', [
            'controller_name' => 'DisponibiliteController',
            'formDispo' => $formDispo->createView(),
        ]);
    }

    /**
     * @Route("/deleteDisponibilite/{id}", name="deleteDisponibilite")
     * @Route("/deleteDisponibilite/{id}/{confirm}", name="deleteDisponibiliteConfirm")
     * @param null $id
     * @param ObjectManager $manager
     * @param DisponibiliteRepository $repoDispo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteDisponibilite($id = null, $confirm = null, ObjectManager $manager, DisponibiliteRepository $repoDispo)
    {
        $dispo = $repoDispo->find($id);
        if ($this->get('security.token_storage')->getToken()->getUser()->getRoles()[0] === "ROLE_ADMIN") {

        } else {
            if ($dispo->getPlanning()->getSecurityUser() != $this->get('security.token_storage')->getToken()->getUser()) {
                dump('ACCES DENIED');
                die;
            }
        }

        if ($confirm) {
            $manager->remove($dispo);
            $manager->flush();
            $id = $dispo->getPlanning()->getID();
            return $this->redirect("/showPlanning/$id");
        }

        foreach ($dispo->creneaux as $creneau) {
            if ($creneau->getEtat() == Creneau::OCCUPER) {
                return $this->render('Disponibilite/confirm.html.twig', [
                    'controller_name' => 'PlanningController',
                    'creneau' => $creneau,
                ]);
            }
        }

        $manager->remove($dispo);
        $manager->flush();
        $id = $dispo->getPlanning()->getID();
        return $this->redirect("/showPlanning/$id");
    }

    /**
     * @Route("/changDisponibilite/{id}", name="changDisponibilite")
     * @param null $id
     * @param DisponibiliteRepository $repoDispo
     * @param Request $request
     * @param ObjectManager $manager
     * @param PlanningRepository $repoPlanning
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changDisponibilite($id = null, DisponibiliteRepository $repoDispo, Request $request, ObjectManager $manager)
    {
        $dispo = $repoDispo->find($id);

        if ($this->get('security.token_storage')->getToken()->getUser()->getRoles()[0] === "ROLE_ADMIN") {

        } else {
            if ($dispo->getPlanning()->getSecurityUser() != $this->get('security.token_storage')->getToken()->getUser()) {
                dump('ACCES DENIED');
                die;
            }
        }

        $formDispo = $this->createForm(DisponibiliteType::class, $dispo);

        $formDispo->handleRequest($request);

        if ($formDispo->isSubmitted() && $formDispo->isValid()) {

            $manager->persist($dispo);
            $manager->flush();
            $idd = $dispo->getId();
            return $this->redirect("/createCreneau/$idd");
        }
        return $this->render('disponibilite/createDisponibilite.html.twig', [
            'controller_name' => 'DisponibiliteController',
            'formDispo' => $formDispo->createView(),
        ]);
    }
}




<?php

namespace App\Controller\Formulaire;


use App\Entity\Formulaire\Question;
use App\Entity\Formulaire\Reponse;
use App\Entity\Formulaire\ReponseDate;
use App\Entity\Formulaire\ReponseNumber;
use App\Entity\Formulaire\ReponseText;

use App\Form\Formulaire\ReponseDateSelectionType;
use App\Form\Formulaire\ReponseDateType;
use App\Form\Formulaire\ReponseNumberSelectionType;
use App\Form\Formulaire\ReponseNumberType;
use App\Form\Formulaire\ReponseNumber2Type;
use App\Form\Formulaire\ReponseLibelleType;
use App\Form\Formulaire\ReponseTimeType;
use App\Repository\Formulaire\DocumentRepository;
use App\Repository\Formulaire\QuestionRepository;
use App\Repository\Formulaire\ReponseDateRepository;
use App\Repository\Formulaire\ReponseNumberRepository;
use App\Repository\Formulaire\ReponseRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ReponseControllerAdmin extends AbstractController
{

    /**
     * @Route("/createReponse/{id}", name="createReponse")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createReponse($id, Request $request, ObjectManager $manager, QuestionRepository $repoQuestion)
    {
        $reponse = new Reponse;

        $formReponse = $this->createForm(ReponseLibelleType::class, $reponse);

        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {
            $question = $repoQuestion->find($id);
            $question->addReponse($reponse);
            $question->setType(Question::CHOICE);
            $manager->persist($question);
            $manager->flush();

            return $this->redirect("/showQuestionAdmin/$id");
        }
        return $this->render('reponse/createReponse.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
        ]);
    }

    /**
     * @Route("/createReponseNumber/{idq}/{idr}", name="createReponseNumber")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createReponseNumber($idq, $idr, Request $request, ObjectManager $manager, QuestionRepository $repoQuestion, ReponseNumberRepository $repoReponse)
    {
        $reponse = $repoReponse->find($idr);
        if ($reponse->getTypeSelection() === ReponseNumber::RANGE) {
            $formReponse = $this->createForm(ReponseNumber2Type::class, $reponse);

        } else {
            $formReponse = $this->createForm(ReponseNumberType::class, $reponse);

        }

        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {

            $question = $repoQuestion->find($idq);
            $question->addReponse($reponse);
            $question->setType(Question::NUMBER);
            $manager->persist($question);
            $manager->flush();
            return $this->redirect("/showQuestionAdmin/$idq");
        }
        return $this->render('reponse/createReponseNumber.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
            'reponse' => $reponse,
        ]);
    }

    /**
     * @Route("/createReponseText/{id}", name="createReponseText")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createReponseText($id, ObjectManager $manager, QuestionRepository $repoQuestion)
    {
        $reponse = new ReponseText();

        $question = $repoQuestion->find($id);
        $question->addReponse($reponse);
        $question->setType(Question::TEXT);
        $manager->persist($question);
        $manager->flush();
        return $this->redirect("/showQuestionAdmin/$id");
    }

    /**
     * @Route("/createReponseDate/{idq}/{idr}", name="createReponseDate")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createReponseDate($idq, $idr, Request $request, ObjectManager $manager, QuestionRepository $repoQuestion, ReponseDateRepository $repoReponse)
    {
        $reponse = $repoReponse->find($idr);

        $question = $repoQuestion->find($idq);

        $question->addReponse($reponse);
        $question->setType(Question::DATE);
        $manager->persist($question);
        $manager->flush();

        if ($reponse->getTypeSelection() === ReponseDate::NULL) {
            return $this->redirect("/showQuestionAdmin/$idq");
        }

        if ($reponse->getTypeSelection() === ReponseDate::INF || $reponse->getTypeSelection() === ReponseDate::SUP) {
            $formReponse = $this->createForm(ReponseDateType::class, $reponse);
        }

        if ($reponse->getTypeSelection() === ReponseDate::INF_delta || $reponse->getTypeSelection() === ReponseDate::SUP_delta) {
            $jour = $_POST['jour'];
            $years = $_POST['years'];
            $date = $_POST['date'];
            $date = new DateTime($date);
            $reponse->setDate($date);
            $reponse->setDelta($jour + $years * 365);
            $reponse->delta();
            $question->addReponse($reponse);
            $manager->persist($question);
            $manager->flush();
            return $this->redirect("/showQuestionAdmin/$idq");
        }


        $formReponse->handleRequest($request);
        if ($formReponse->isSubmitted()) {

            if ($reponse->getTypeSelection() === ReponseDate::INF || $reponse->getTypeSelection() === ReponseDate::SUP) {
                if ($reponse->getDate() === null) {
                    $reponse->setDate(new DateTime());
                    $reponse->actu = true;
                }
            }
            $question = $repoQuestion->find($idq);
            $question->addReponse($reponse);
            $manager->persist($question);
            $manager->flush();
            return $this->redirect("/showQuestionAdmin/$idq");
        }

        return $this->render('reponse/createReponseDate.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
        ]);
    }


    /**
     * @Route("/changReponse/{idq}/{idr}", name="changReponse")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function changReponse($idq, $idr, Request $request, ObjectManager $manager, ReponseRepository $repoReponse)
    {
        $reponse = $repoReponse->find($idr);

        $formReponse = $this->createForm(ReponseLibelleType::class, $reponse);

        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {
            $manager->persist($reponse);
            $manager->flush();
            return $this->redirect("/showQuestionAdmin/$idq");
        }
        return $this->render('reponse/createReponse.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
        ]);
    }


    /**
     * @Route("/changReponseNumber/{idq}/{idr}", name="changReponseNumber")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function changReponseNumber($idq, $idr, Request $request, ObjectManager $manager, ReponseNumberRepository $repoReponseNumber)
    {
        $reponse = $repoReponseNumber->find($idr);

        if ($reponse->getTypeSelection() === ReponseNumber::RANGE) {
            $formReponse = $this->createForm(ReponseNumber2Type::class, $reponse);
        } else {
            $formReponse = $this->createForm(ReponseNumberType::class, $reponse);
        }
        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {
            $manager->persist($reponse);
            $manager->flush();
            return $this->redirect("/showQuestionAdmin/$idq");
        }
        return $this->render('reponse/createReponseNumber.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
            'reponse' => $reponse,
        ]);
    }

    /**
     * @Route("/deleteReponse/{idq}/{idr}", name="deleteReponse")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function deleteReponse($idq, $idr, ObjectManager $manager, ReponseRepository $repoReponse)
    {
        $reponse = $repoReponse->find($idr);
        $manager->remove($reponse);
        $manager->flush();
        return $this->redirect("/showQuestionAdmin/$idq");

    }

    /**
     * @Route("/addTimeRdv/{idq}/{idr}", name="addTimeRdv")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function addTimeRdv($idq, $idr, Request $request, ObjectManager $manager, ReponseRepository $repoReponse)
    {
        $reponse = $repoReponse->find($idr);

        $formReponse = $this->createForm(ReponseTimeType::class, $reponse);

        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {
            $manager->persist($reponse);
            $manager->flush();
            return $this->redirect("/showQuestionAdmin/$idq");
        }
        return $this->render('reponse/addTimeRdv.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
        ]);
    }

    /**
     * @Route("/assignDocument/{idq}/{idr}", name="assignDocument")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function assignDocument($idq, $idr, ReponseRepository $repoReponse, DocumentRepository $repoDocument, ObjectManager $manager)
    {
        $reponse = $repoReponse->find($idr);
        $idd = $_GET['document'];
        $document = $repoDocument->find($idd);
        $reponse->addDocument($document);
        $manager->persist($reponse);
        $manager->flush();
        return $this->redirect("/showQuestionAdmin/$idq");
    }

    /**
     * @Route("/selectDocument/{idq}/{idr}", name="selectDocument")
     */
    public function selectDocument($idq, $idr, DocumentRepository $repoDocument, ReponseRepository $repoReponse, QuestionRepository $repoQuestion)
    {
        $question = $repoQuestion->find($idq);
        $reponse = $repoReponse->find($idr);
        $repoDocument = $repoDocument->findall();
        return $this->render('reponse/selectDocument.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'reponse' => $reponse,
            'repoDocument' => $repoDocument,
            'question' => $question,

        ]);
    }

    /**
     * @Route("/removeDocument/{idq}/{idr}/{idd}", name="removeDocument")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function removeDocument($idq, $idr, $idd, ObjectManager $manager, DocumentRepository $repoDocument, ReponseRepository $repoReponse)
    {
        $document = $repoDocument->find($idd);
        $reponse = $repoReponse->find($idr);
        $reponse->removeDocument($document);
        $manager->persist($reponse);
        $manager->flush();
        return $this->redirect("/showQuestionAdmin/$idq");
    }

    /**
     * @Route("/selectTypeSelectionNumber/{id}", name="selectTypeSelectionNumber")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function selectTypeSelectionNumber($id, ObjectManager $manager, Request $request)
    {
        $reponse = new ReponseNumber();

        $formReponse = $this->createForm(ReponseNumberSelectionType::class, $reponse);

        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {
            $reponse->setMin(0);
            $manager->persist($reponse);
            $manager->flush();
            return $this->redirect("/createReponseNumber/$id/$reponse->id");
        }
        return $this->render('reponse/SelectTypeSelection.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
        ]);
    }

    /**
     * @Route("/selectTypeSelectionDate/{id}", name="selectTypeSelectionDate")
     * @Route("/selectTypeSelectionDate/{id}/{idr}", name="selectTypeSelectionDateId")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function selectTypeSelectionDate($id, $idr = null, ObjectManager $manager, Request $request, ReponseDateRepository $repoReponse)
    {
        if ($idr) {
            $reponse = $repoReponse->find($idr);
        } else {
            $reponse = new ReponseDate();

        }

        $formReponse = $this->createForm(ReponseDateSelectionType::class, $reponse);

        $formReponse->handleRequest($request);

        if ($formReponse->isSubmitted() && $formReponse->isValid()) {
            $manager->persist($reponse);
            $manager->flush();
            if ($reponse->getTypeSelection() === ReponseDate::SUP_delta || $reponse->getTypeSelection() === ReponseDate::INF_delta) {
                return $this->render('reponse/createReponseDateDelta.html.twig', [
                    'controller_name' => 'ReponseControllerAdmin',
                    'formReponse' => $formReponse->createView(),
                    'id' => $id,
                    'reponse' => $reponse->id,
                ]);
            } else {
                return $this->redirect("/createReponseDate/$id/$reponse->id");
            }
        }
        return $this->render('reponse/SelectTypeSelection.html.twig', [
            'controller_name' => 'ReponseControllerAdmin',
            'formReponse' => $formReponse->createView(),
        ]);
    }
}

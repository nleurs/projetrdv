<?php

namespace App\Form\Planning;

use App\Entity\Planning\Disponibilite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DisponibiliteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hDebut',
                TimeType::class,array('minutes' => ['00','15','30','45']))
            ->add('hFin',
                TimeType::class,array('minutes' => ['00','15','30','45']))
            ->add('date',
                DateType::class,
                array('format' => 'dd-MM-yyyy','data' => new \DateTime("now"))
            );;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Disponibilite::class,
        ]);
    }
}

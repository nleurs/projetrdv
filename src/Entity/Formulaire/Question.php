<?php

namespace App\Entity\Formulaire;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Formulaire\QuestionRepository")
 */
class Question
{

    const CHOICE = "CHOICE";
    const NUMBER = "NUMBER";
    const TEXT = "TEXT";
    const DATE = "DATE";
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @var Reponse[]
     * @ORM\OneToMany(targetEntity="Reponse", cascade={"persist","remove"}, mappedBy="question")
     */
    public $reponses;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $type;

    /**
     * @ORM\Column(type="string", nullable=true, length=40)
     */
    public $code;


    public function __construct()
    {
        $this->reponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $question): self
    {
        $this->libelle = $question;

        return $this;
    }

    public function addReponse(Reponse $reponse)
    {
        $this->reponses[] = $reponse;
        $reponse->setQuestion($this);
    }

    /**
     * @return Reponse[]|ArrayCollection
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getClass()
    {
        return "Question";
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }
}

<?php

namespace App\Entity\Formulaire;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Formulaire\FormRepository")
 */
class Form
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $FirstQuestion;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstQuestion()
    {
        return $this->FirstQuestion;
    }

    /**
     * @param mixed $FirstQuestion
     */
    public function setFirstQuestion($FirstQuestion): void
    {
        $this->FirstQuestion = $FirstQuestion;
    }

    public function getTable($table, $x, $y, $courant, $repoQuestion)
    {
        array_push($table, array('element' => $courant, 'X' => $x, 'Y' => $y));
        $x += 1;
        $pos = sizeof($table) - 1;
        foreach ($courant->reponses as $r) {
            array_push($table, array('element' => $r, 'X' => $x, 'Y' => $y, 'question' => $pos));
            if ($r->getsuivante()) {
                $table = $this->getTable($table, $x + 1, $y, $repoQuestion->find($r->getSuivante()), $repoQuestion);
                $lastcoord = end($table);
                $y = $lastcoord["Y"] + 1;
            } else {
                $y += 1;
            }
        }
        return $table;
    }
}
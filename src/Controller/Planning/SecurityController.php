<?php

namespace App\Controller\Planning;

use App\Entity\Planning\SecurityUser;

use App\Form\Planning\LoginType;
use App\Form\Planning\RegisterType;

use App\Repository\Planning\SecurityUserRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $securityUser = new SecurityUser();
        $form = $this->createForm(RegisterType::class, $securityUser);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($securityUser, "null");
            $securityUser->setPassword($hash);
            $manager->persist($securityUser);
            $manager->flush();
            $id = $securityUser->getId();
            return $this->redirect("/createPlanning/{$id}");
        }
        return $this->render('security/register.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/login", name="security_login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $usecurityUser = new SecurityUser();
        $form = $this->createForm(LoginType::class, $usecurityUser);
        $form->handleRequest($request);

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout()
    {
    }

    /**
     * @Route("/listGest", name="listGest")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function listGest(SecurityUserRepository $repoSecurityUser)
    {
        $repoSecurityUser = $repoSecurityUser->findall();
        dump($repoSecurityUser);
        die;
    }

}

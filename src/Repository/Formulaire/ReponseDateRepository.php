<?php

namespace App\Repository\Formulaire;

use App\Entity\Formulaire\ReponseDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReponseDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReponseDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReponseDate[]    findAll()
 * @method ReponseDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseDateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReponseDate::class);
    }

//    /**
//     * @return ReponseDate[] Returns an array of ReponseDate objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReponseDate
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

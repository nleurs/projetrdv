<?php

namespace App\Entity\Formulaire;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\Formulaire\ReponseRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"reponse" = "Reponse", "reponseNumber" = "ReponseNumber", "reponseText" = "ReponseText", "reponseDate" = "ReponseDate"})
 *
 */
class Reponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="reponse")
     * @ORM\JoinColumn(name="question_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $question;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $suivante;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $besoinRdv;

    /**
     * @var Document[]
     * @ORM\ManyToMany(targetEntity="Document", cascade={"persist","remove"})
     */

    public $documents;

    /**
     * @var UserReponse[]
     * @ORM\OneToMany(targetEntity="UserReponse", cascade={"persist","remove"}, mappedBy="reponse")
     */
    public $UserReponses;

    /**
     * @ORM\Column(type="string", nullable=true, length=40)
     */
    public $code;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
        $this->UserReponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $reponse): self
    {
        $this->libelle = $reponse;

        return $this;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getSuivante(): ?int
    {
        return $this->suivante;
    }

    public function setSuivante($suivante): self
    {
        $this->suivante = $suivante;

        return $this;
    }

    public function addDocument(Document $d)
    {
        $cpt = 0;
        foreach ($this->documents as $document) {
            if ($document === $d) {
                $cpt++;
            }
        }
        if ($cpt === 0) {
            $this->documents[] = $d;
            $d->setReponse($this);
        }
    }

    public function removeDocument(Document $d)
    {
        foreach ($this->documents as $document) {
            if ($document === $d) {
                $this->documents->removeElement($document);
            }
        }
    }

    /**
     * @return Document[]|ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function getBesoinRdv(): ?int
    {
        return $this->besoinRdv;
    }

    public function setBesoinRdv(?int $besoinRdv): self
    {
        $this->besoinRdv = $besoinRdv;

        return $this;
    }

    public function getClass()
    {
        return "Reponse";
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }
}


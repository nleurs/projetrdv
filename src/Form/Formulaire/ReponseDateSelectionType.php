<?php

namespace App\Form\Formulaire;

use App\Entity\Formulaire\ReponseDate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseDateSelectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code',TextType::class,['label' => 'Code de la réponse:'])
            ->add('typeSelection',ChoiceType::class,['label' => 'par rapport à une valeur saisie la date doit étre:', 'choices'  => array(
                'pas de vérification' => ReponseDate::NULL,
                'inférieur strict à une date particuliére' => ReponseDate::INF,
                'supérieur strict à une date particuliére' => ReponseDate::SUP,
                'inférieur strict à la date ou le formulaire est rempli + un delta' => ReponseDate::INF_delta,
                'supérieur strict à la date ou le formulaire est rempli - un delta' => ReponseDate::SUP_delta,

            ),])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReponseDate::class,
        ]);
    }
}

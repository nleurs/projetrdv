<?php
/**
 * Created by PhpStorm.
 * User: leurs
 * Date: 24/10/2018
 * Time: 16:30
 */

namespace App\Controller\Formulaire;

use App\Entity\Formulaire\Document;

use App\Form\Formulaire\DocumentType;

use App\Repository\Formulaire\DocumentRepository;

use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class DocumentControllerAdmin extends AbstractController
{

    /**
     * @Route("/createDocument", name="createDocument")
     * @Route("/createDocument/{id}", name="createDocumentId")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function createDocument($id =null,Request $request, ObjectManager $manager)
    {
        $document = new Document;
        $formDocument = $this->createForm(DocumentType::class, $document);

        $formDocument->handleRequest($request);

        if ($formDocument->isSubmitted() && $formDocument->isValid()) {
            $manager->persist($document);
            $manager->flush();

            if($id)
            {
                return $this->redirect("/showQuestionAdmin/$id");
            }
            return $this->redirect("/formAdmin");
        }
        return $this->render('document/createDocument.html.twig', [
            'controller_name' => 'DocumentControllerAdmin',
            'formDocument' => $formDocument->createView(),
        ]);
    }

    /**
     * @Route("/deleteDocument/{idd}", name="deleteDocument")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteDocument($idd,ObjectManager $manager, DocumentRepository $repoDocument)
    {
        $document = $repoDocument->find($idd);
        $manager->remove($document);
        $manager->flush();
        return $this->redirect("/formAdmin");

    }

}
<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181022095020 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form ADD id_first_question INT NOT NULL, DROP n_question');
        $this->addSql('ALTER TABLE reponse ADD id_question INT NOT NULL, ADD id_suivante INT DEFAULT NULL, DROP suivante, DROP question');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form ADD n_question INT DEFAULT NULL, DROP id_first_question');
        $this->addSql('ALTER TABLE reponse ADD question INT DEFAULT NULL, DROP id_question, CHANGE id_suivante suivante INT DEFAULT NULL');
    }
}

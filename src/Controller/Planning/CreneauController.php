<?php

namespace App\Controller\Planning;

use App\Entity\Planning\Creneau;

use App\Repository\Planning\CreneauRepository;
use App\Repository\Planning\DisponibiliteRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CreneauController extends AbstractController
{
    /**
     * @Route("/createCreneau/{id}", name="createCreneau")
     * @param null $id
     * @param ObjectManager $manager
     * @param DisponibiliteRepository $repoDispo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createCreneau($id = null, ObjectManager $manager, DisponibiliteRepository $repoDispo, CreneauRepository $repoCreneaux)
    {
        $dispo = $repoDispo->find($id);
        foreach ($dispo->creneaux as $c) {
            $c->setDisponibilite(null);
        }

        $parcour = $dispo->getHDebut();
        while ($parcour != $dispo->getHFin()) {
            dump($parcour);
            if ($repoCreneaux->findBy(array('hDebut' => $parcour)) != null) {
                $creneau = $repoCreneaux->findBy(array('disponibilite' => $dispo, 'hDebut' => $parcour));
                $creneau[0]->setDisponibilite($dispo);
                $manager->persist($creneau[0]);
                $manager->flush();
                $parcour->modify('+15 minute');
            } else {
                $creneau = new Creneau();
                $creneau->setHDebut($parcour);
                $creneau->setEtat(Creneau::LIBRE);
                $creneau->setDisponibilite($dispo);

                $manager->persist($creneau);
                $manager->flush();
                $parcour->modify('+15 minute');
            }
        }

        dump($parcour);
        return $this->redirect("/cleanCreneau");
    }

    /**
     * @Route("/cleanCreneau", name="cleanCreneau")
     */
    public function cleanCreneau(ObjectManager $manager, CreneauRepository $repoCreneau)
    {
        $creneaux = $repoCreneau->findBy(array('disponibilite' => null));
        dump($creneaux);
//        die;
        return $this->redirect("/showPlanning");
    }


    /**
     * @Route("/deleteCreneau/{id}", name="deleteCreneau")
     * @Route("/deleteCreneau/{id}/{confirm}", name="deleteCreneauConfirm")
     */
    public function deleteCreneau($id = null, $confirm = null, ObjectManager $manager, CreneauRepository $repoCreneau, DisponibiliteRepository $repoDispo)
    {
        $creneau = $repoCreneau->find($id);
        if ($this->get('security.token_storage')->getToken()->getUser()->getRoles()[0] === "ROLE_ADMIN") {

        } else {
            if ($creneau->getDisponibilite()->getPlanning()->getSecurityUser() != $this->get('security.token_storage')->getToken()->getUser()) {
                dump('ACCES DENIED');
                die;
            }
        }

        $dispo = $repoDispo->find($creneau->getDisponibilite());

        if ($confirm) {
            $creneau->setEtat(Creneau::LIBRE);
        }

        if ($creneau->getEtat() == Creneau::OCCUPER) {
            return $this->render('Creneau/confirm.html.twig', [
                'controller_name' => 'PlanningController',
                'creneau' => $creneau,
            ]);
        }


        $manager->remove($creneau);
        $manager->flush();
        $id = $creneau->getDisponibilite()->getPlanning()->getID();

        $creneaux = $repoCreneau->findBy(array('disponibilite' => $dispo));
        if ($creneaux == null) {
            $manager->remove($dispo);
            $manager->flush();
        }
        return $this->redirect("/showPlanning/$id");


    }
}


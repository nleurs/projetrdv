<?php

namespace App\Form\Formulaire;

use App\Entity\Formulaire\ReponseNumber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseNumberSelectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code',TextType::class,['label' => 'Code de la réponse:'])
            ->add('typeSelection',ChoiceType::class,['label' => 'par rapport à une valeur saisie le résultat doit étre:', 'choices'  => array(
                'égal' => ReponseNumber::EQUALS,
                'inférieur ou égal' => ReponseNumber::INF,
                'supérieur ou égal' => ReponseNumber::SUP,
                'entre un minimum et un maximun' => ReponseNumber::RANGE,
                'inférieur strict' => ReponseNumber::INF_str,
                'supérieur strict' => ReponseNumber::SUP_str,

            ),])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReponseNumber::class,
        ]);
    }
}

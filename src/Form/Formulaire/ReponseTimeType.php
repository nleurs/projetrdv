<?php

namespace App\Form\Formulaire;

use App\Entity\Formulaire\Reponse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('besoin_rdv',ChoiceType::class,['label' => 'durée du rendez-vous nécessaire à partir de cette reponse:', 'choices'  => array(
                'pas de rendez vous' => 0,
                '15 min' => 15,
                '30 min' => 30,
                '1 h' => 60
            ),])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reponse::class,
        ]);
    }
}

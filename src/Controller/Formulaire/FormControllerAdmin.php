<?php

namespace App\Controller\Formulaire;

use App\Entity\Formulaire\Form;

use App\Repository\Formulaire\DocumentRepository;
use App\Repository\Formulaire\QuestionRepository;
use App\Repository\Formulaire\FormRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class FormControllerAdmin extends AbstractController
{
    /**
     * @Route("/formAdmin", name="formAdmin")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function index(QuestionRepository $repoQuestion, DocumentRepository $repoDocument)
    {
        $repoQuestion = $repoQuestion->findall();
        $repoDocument = $repoDocument->findall();
        return $this->render('form/index.html.twig', [
            'controller_name' => 'FormControllerAdmin',
            'repoQuestion' => $repoQuestion,
            'repoDocument' => $repoDocument,
        ]);
    }

    /**
     * @Route("/validateCreateForm", name="validateCreateForm")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function validateCreateForm()
    {
        return $this->render('form/validateCreateForm.html.twig', [
            'controller_name' => 'FormControllerAdmin',
        ]);
    }

    /**
     * @Route("/createForm", name="createForm")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function newForm(ObjectManager $manager, FormRepository $repoForm, QuestionRepository $repoQuestion)
    {
        $form = $repoForm->findOneBy([]);
        $question = $repoQuestion->findall();

        if (!$form) {
            $form = new Form();
        }
        foreach ($question as $question) {

            $manager->remove($repoQuestion->findOneBy([]));
            $manager->flush();
        }

        $manager->remove($form);
        $manager->flush();
        $form = new Form();
        $manager->persist($form);
        $manager->flush();
        return $this->redirectToRoute('createQuestion');
    }

    /**
     * @Route("/showSchema", name="showSchema")
     * @Security("is_granted('ROLE_GEST')")
     */
    public function showSchema(QuestionRepository $repoQuestion, FormRepository $repoForm)
    {
        $form = $repoForm->findOneBy([]);
        $table = array();
        $x = $y = 0;
        $courant = $repoQuestion->find($form->getFirstQuestion());
        $table = $form->getTable($table, $x, $y, $courant, $repoQuestion);
        return $this->render('form/showSchema.html.twig', [
            'controller_name' => 'FormControllerAdmin',
            'table' => $table,
        ]);
    }
}

<?php

namespace App\Entity\Planning;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Planning\PlanningRepository")
 */
class Planning
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Disponibilite[]
     * @ORM\OneToMany(targetEntity="Disponibilite", cascade={"persist","remove"}, mappedBy="planning")
     */

    public $disponibilites;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Planning\SecurityUser", mappedBy="planning", cascade={"persist", "remove"})
     */
    private $securityUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSecurityUser(): ?SecurityUser
    {
        return $this->securityUser;
    }

    public function setSecurityUser(?SecurityUser $securityUser): self
    {
        $this->securityUser = $securityUser;

        // set (or unset) the owning side of the relation if necessary
        $newPlanning = $securityUser === null ? null : $this;
        if ($newPlanning !== $securityUser->getPlanning()) {
            $securityUser->setPlanning($newPlanning);
        }

        return $this;
    }


}

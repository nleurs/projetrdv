<?php

namespace App\Controller\Formulaire;

use App\Entity\Formulaire\Question;
use App\Entity\Formulaire\ReponseDate;
use App\Entity\Formulaire\ReponseNumber;
use App\Entity\Formulaire\User;
use App\Entity\Formulaire\UserReponse;
use App\Form\Formulaire\UserReponseDateType;
use App\Form\Formulaire\UserReponseTextType;
use App\Form\Formulaire\UserReponseValueType;
use App\Repository\Formulaire\FormRepository;
use App\Repository\Formulaire\QuestionRepository;
use App\Repository\Formulaire\ReponseRepository;
use App\Repository\Formulaire\UserReponseRepository;
use App\Repository\Formulaire\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class UserController extends AbstractController
{

    /**
     * @Route("/formUser", name="formUser")
     */
    public function formUser()
    {
        return $this->render('User/index.html.twig', [
            'controller_name' => 'UserControllerAdmin',

        ]);
    }

    /**
     * @Route("/showQuestionUser/[idu}", name="showQuestionUser")
     * @Route("/showQuestionUser/{idu}/{id}", name="showQuestionUserId")
     */
    public function showQuestionUser($id = null, $idu, QuestionRepository $repoQuestion, ReponseRepository $repoReponse, Request $request, ObjectManager $manager, UserRepository $repoUser, UserReponseRepository $repoUserReponse)
    {
        $user = $repoUser->find($idu);
        $listRep = $repoUserReponse->findall();

        if ($id === null) {
            return $this->render('user/showEndUser.html.twig', [
                'controller_name' => 'UserController',
                'userReponse' => $listRep,
                'user' => $user,
            ]);
        }

        $question = $repoQuestion->find($id);

        if ($question->type === Question::CHOICE) {
            return $this->render('user/showQuestionUser.html.twig', [
                'controller_name' => 'UserController',
                'userReponse' => $listRep,
                'question' => $question,
                'user' => $user,
                "id" => $id,
            ]);
        }

        if ($question->type === Question::NUMBER) {

            $userReponse = new UserReponse();

            foreach ($user->userReponses as $r) {
                if ($r->getReponse()->getQuestion()->id == $id) {
                    $userReponse = $r;
                    $user->decNbReponse();
                    while ($r->getReponse()->getSuivante() != null) {
                        $user->decNbReponse();
                        $rep = $repoReponse->findBy(array('question' => $r->getReponse()->getSuivante()));
                        $r = $repoUserReponse->findOneBy(array('reponse' => $rep));
                        $manager->remove($r);
                        $manager->flush();
                    }
                }
            }
            $form = $this->createForm(UserReponseValueType::class, $userReponse);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $reponses = $repoReponse->findBy(['question' => $id]);
                $reponse = new ReponseNumber();

                $user = $repoUser->find($idu);
                $userReponse->setOrdre($user);
                $userReponse->setUser($user);
                $manager->persist($userReponse);
                $manager->flush();

                $reponse = $reponse->testType($reponses, $userReponse->getValue());

                if ($reponse == null) {
                    return $this->render('user/invalide.html.twig', [
                        'controller_name' => 'UserController',
                    ]);
                }

                $reponse = $repoReponse->find($reponse->id);
                $userReponse->setReponse($reponse);
                $user->addReponse($userReponse);
                $user->checkMax($reponse);
                $manager->persist($user);
                $manager->flush();
                $id = $reponse->getSuivante();
                return $this->redirect("/showQuestionUser/$idu/$id");
            }
            return $this->render('user/showQuestionUserInt.html.twig', [
                'controller_name' => 'UserController',
                'question' => $question,
                'form' => $form->createView(),
                'user' => $user,
                'userReponse' => $listRep,

            ]);
        }

        if ($question->type === Question::TEXT) {
            $userReponse = new UserReponse();

            foreach ($user->userReponses as $r) {
                if ($r->getReponse()->getQuestion()->id == $id) {
                    $userReponse = $r;
                    while ($r->getReponse()->getSuivante() != null) {
                        $user->decNbReponse();
                        $rep = $repoReponse->findBy(array('question' => $r->getReponse()->getSuivante()));
                        $r = $repoUserReponse->findOneBy(array('reponse' => $rep));
                        $manager->remove($r);
                        $manager->flush();
                    }
                }
            }

            $form = $this->createForm(UserReponseTextType::class, $userReponse);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $reponse = $repoReponse->findOneBy(['question' => $id]);
                $user = $repoUser->find($idu);
                $userReponse->setOrdre($user);
                $userReponse->setUser($user);
                $userReponse->setReponse($reponse);

                $user->addReponse($userReponse);

                $user->checkMax($reponse);

                $manager->persist($user);
                $manager->flush();
                $id = $reponse->getSuivante();


                return $this->redirect("/showQuestionUser/$idu/$id");
            }
            return $this->render('user/showQuestionUserText.html.twig', [
                'controller_name' => 'UserController',
                'question' => $question,
                'form' => $form->createView(),
                'user' => $user,
                'userReponse' => $listRep,

            ]);
        }

        if ($question->type === Question::DATE) {
            foreach ($question->reponses as $rep) {
                if ($rep->actu === true) {
                    $rep->reload();
                }
            }
            $userReponse = new UserReponse();

            foreach ($user->userReponses as $r) {

                if ($r->getReponse()->getQuestion()->id == $id) {
                    $userReponse = $r;
                    $user->decNbReponse();
                    while ($r->getReponse()->getSuivante() != null) {
                        $user->decNbReponse();
                        $rep = $repoReponse->findBy(array('question' => $r->getReponse()->getSuivante()));
                        $r = $repoUserReponse->findOneBy(array('reponse' => $rep));
                        $manager->remove($r);
                        $manager->flush();
                    }
                }
            }

            $form = $this->createForm(UserReponseDateType::class, $userReponse);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $reponses = $repoReponse->findBy(['question' => $id]);
                $reponse = new ReponseDate();

                $user = $repoUser->find($idu);
                $userReponse->setOrdre($user);
                $userReponse->setUser($user);
                $manager->persist($userReponse);
                $manager->flush();

                $reponse = $reponse->testType($reponses, $userReponse->getDate());

                if ($reponse == null) {
                    return $this->render('user/invalide.html.twig', [
                        'controller_name' => 'UserController',
                    ]);
                }

                $reponse = $repoReponse->find($reponse->id);
                $userReponse->setReponse($reponse);
                $user->addReponse($userReponse);
                $user->checkMax($reponse);
                $manager->persist($user);
                $manager->flush();
                $id = $reponse->getSuivante();
                return $this->redirect("/showQuestionUser/$idu/$id");
            }
            return $this->render('user/showQuestionUserDate.html.twig', [
                'controller_name' => 'UserController',
                'question' => $question,
                'form' => $form->createView(),
                'user' => $user,
                'userReponse' => $listRep,

            ]);
        }

    }


    /**
     * @Route("/createUser", name="createUser")
     */
    public function createUser(ObjectManager $manager, FormRepository $repoForm)
    {
        $user = new User();
        $manager->persist($user);
        $manager->flush();
        $form = $repoForm->findOneBy([]);
        $idu = $user->getId();
        $id = $form->getFirstQuestion();

        return $this->redirect("/showQuestionUser/$idu/$id");
    }

    /**
     * @Route("/assignChoix/{idu}/{idq}", name="assignChoix")
     */
    public function assignChoix($idu, $idq, ReponseRepository $repoReponse, ObjectManager $manager, UserRepository $repoUser, UserReponseRepository $repoUserReponse)
    {
        $id = $_GET['choix'];
        $user = $repoUser->find($idu);
        $reponse = $repoReponse->find($id);
        $userReponse = new UserReponse();

        foreach ($user->userReponses as $r) {
            if ($r->getReponse()->getQuestion()->id == $idq) {
                $userReponse = $r;
                $user->decNbReponse();
                while ($r->getReponse()->getSuivante() != null) {
                    $user->decNbReponse();
                    $rep = $repoReponse->findBy(array('question' => $r->getReponse()->getSuivante()));
                    $r = $repoUserReponse->findOneBy(array('reponse' => $rep));
                    $manager->remove($r);
                    $manager->flush();
                }
            }
        }
        $userReponse->setOrdre($user);
        $userReponse->setUser($user);
        $userReponse->setReponse($reponse);
        $user->addReponse($userReponse);

        $user->checkMax($reponse);
        $manager->persist($user);
        $manager->flush();
        $id = $reponse->getSuivante();
        return $this->redirect("/showQuestionUser/$idu/$id");
    }

    /**
     * @Route("/showProfil/{id}", name="showProfil")
     */
    public function showProfil($id = null, UserRepository $repoUser, UserReponseRepository $repoUserReponse)
    {
        $user = $repoUser->find($id);
        $listRep = $repoUserReponse->findBy(array('user' =>$user));
        return $this->render('user/showProfil.html.twig', [
            'controller_name' => 'UserController',
            'user' => $user,
            'userReponse' => $listRep,
        ]);
    }
}

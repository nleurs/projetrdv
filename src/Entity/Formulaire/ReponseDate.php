<?php

namespace App\Entity\Formulaire;

use DateInterval;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Formulaire\ReponseDateRepository")
 */
class ReponseDate extends Reponse
{

    const NULL = "NULL";
    const INF = "INF";
    const SUP = "SUP";
    const INF_delta = "INF_delta";
    const SUP_delta = "SUP_delta";
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $delta;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeSelection;

    /**
     * @ORM\Column(type="boolean")
     */
    public $actu = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getDelta(): ?int
    {
        return $this->delta;
    }

    public function setDelta(int $delta): self
    {
        $this->delta = $delta;

        return $this;
    }

    public function getTypeSelection(): ?string
    {
        return $this->typeSelection;
    }

    public function setTypeSelection(string $typeSelection): self
    {
        $this->typeSelection = $typeSelection;

        return $this;
    }

    public function delta()
    {
        if ($this->date == null) {
            $this->date = new DateTime();
        }
        if ($this->getTypeSelection() === ReponseDate::SUP_delta) {
            $this->date->sub(new DateInterval("P{$this->delta}D"));
        }
        if ($this->getTypeSelection() === ReponseDate::INF_delta) {
            $this->date->add(new DateInterval("P{$this->delta}D"));
        }
    }

    public function testType($reponses, $date)
    {
        foreach ($reponses as $r) {
            switch ($r->typeSelection) {
                case ReponseDate::INF_delta:
                    if ($r->getDate() > $date)
                        return $r;
                    break;
                case ReponseDate::SUP_delta:
                    if ($r->getDate() < $date)
                        return $r;
                    break;
                case ReponseDate::INF:
                    if ($r->getDate() > $date)
                        return $r;
                    break;
                case ReponseNumber::SUP:
                    if ($r->getDate() < $date)
                        return $r;
                    break;
            }
        }
    }

    public function Reload()
    {
        $this->date = new DateTime();
        if ($this->typeSelection == 'INF_delta' or $this->typeSelection == "SUP_delta") {
            $this->delta();
        }
    }
}

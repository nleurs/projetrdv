<?php

namespace App\Controller;

use App\Entity\Planning\Creneau;
use App\Repository\Formulaire\UserRepository;
use App\Repository\Planning\DisponibiliteRepository;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RendezVousController extends AbstractController
{
    /**
     * @Route("/takeRDV/{id}", name="takeRDV")
     */
    public function takeRDV($id = null, UserRepository $repoUser, DisponibiliteRepository $repoDispo, ObjectManager $manager)
    {
        $user = $repoUser->find($id);
        $nbCreneau = $user->getTimeRdv() / 15;
        $cpt = 0;
        $memoire = array();
        $listDispo = $repoDispo->findall();
        foreach ($listDispo as $dispo) {
            $today = new DateTime('+ 7 day');
            $today = $today->format('d-m-y');
            $dateDispo = ($dispo->getDate()->format('d-m-y'));
            if ($today == $dateDispo) {
                foreach ($dispo->creneaux as $creneau) {
                    if ($creneau->getEtat() == Creneau::LIBRE) {
                        $cpt++;
                        array_push($memoire, $creneau);
                    }
                    if ($creneau->getEtat() == Creneau::OCCUPER) {
                        $memoire = array();
                        $cpt = 0;
                    }
                    if ($cpt == $nbCreneau) {
                        break;
                    }
                }
            }
        }
        foreach ($memoire as $creneau) {
            $creneau->setEtat(Creneau::OCCUPER);
            $creneau->setUser($user);
            $manager->persist($creneau);
            $manager->flush();
        }


        if ($memoire == null) {
            return $this->render('RendezVous/erreur.html.twig', [
                'controller_name' => 'RendezVousController',
            ]);
        } else {
            return $this->render('RendezVous/showRendezVous.html.twig', [
                'controller_name' => 'RendezVousController',
                'rendezVous' => $memoire,
            ]);
        }
    }
}

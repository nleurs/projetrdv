<?php

namespace App\Entity\Formulaire;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Formulaire\ReponseNumberRepository")
 */
class ReponseNumber extends Reponse
{
    const EQUALS = "EQUALS";
    const INF = "INF";
    const SUP = "SUP";
    const RANGE = "RANGE";
    const INF_str = "INF_str";
    const SUP_str = "SUP_str";

    /**
     * @ORM\Column(type="integer")
     */
    private $min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max;

    /**
     * @ORM\Column(type="string")
     */
    private $typeSelection;

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getTypeSelection()
    {
        return $this->typeSelection;
    }

    public function setTypeSelection(string $typeSelection)
    {
        $this->typeSelection = $typeSelection;

        return $this;
    }

    public function testType($reponses, $value)
    {
        foreach ($reponses as $r) {
            switch ($r->typeSelection) {
                case ReponseNumber::EQUALS:
                    if ($r->getMin() == $value)
                        return $r;
                    break;
                case ReponseNumber::INF:
                    if ($r->getMin() >= $value)
                        return $r;
                    break;
                case ReponseNumber::SUP:
                    if ($r->getMin() <= $value)
                        return $r;
                    break;
                case ReponseNumber::RANGE:
                    if ($r->getMin() <= $value AND $r->getMax() >= $value)
                        return $r;
                    break;
                case ReponseNumber::INF_str:
                    if ($r->getMin() > $value)
                        return $r;
                    break;
                case ReponseNumber::SUP_str:
                    if ($r->getMin() < $value)
                        return $r;
                    break;
            }
        }
    }
}



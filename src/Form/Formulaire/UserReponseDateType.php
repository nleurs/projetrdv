<?php

namespace App\Form\Formulaire;

use App\Entity\Formulaire\UserReponse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserReponseDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date',
                DateType::class,
                array('years' => range(date('Y') - 21, date('Y') + 20),
                    'format' => 'dd-MM-yyyy','label' => ' ')
            );
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserReponse::class,
        ]);
    }
}
